require 'sinatra/base'
require 'sinatra/reloader'
require 'sqlite3'
require 'active_record'


ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3', 
  database: 'goroku.sqlite3'
)

class Chapter < ActiveRecord::Base
end


class Route < Sinatra::Base
  set :bind, '0.0.0.0'
  set :port, 37564
  enable :sessions

  get "/" do
    @results = {}
    unless session[:result_id].blank?
      session[:result_id].each do |id|
        @results[id] = context_of id
      end
    end

    erb :index
  end

  post "/form" do
    if params[:word].blank?
      session[:result_id] = []
    else
      word = params[:word]    
      session[:result_id] = search word
    end

    redirect to('/')
  end

  # return Chapter instances include word
  def search word
    res = Chapter.where("section LIKE ?", "%#{word}%")    
    return res.map {|node| node.id}
  end

  def context_of id
    res = Chapter.where(id: (id-2)..(id+2))
  end

  def self.start
    run!
  end
end
